package br.com.digio.androidtest.matchers

import androidx.annotation.StringRes
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withText

fun ViewInteraction.isDisplayed() =
    check(matches(androidx.test.espresso.matcher.ViewMatchers.isDisplayed()))

fun ViewInteraction.matchText(text: String) = check(matches(withText(text)))

fun ViewInteraction.matchText(@StringRes textId: Int) = check(matches(withText(textId)))