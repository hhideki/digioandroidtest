package br.com.digio.androidtest.ui

import androidx.lifecycle.Lifecycle
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry
import br.com.digio.androidtest.MockServerUtil
import br.com.digio.androidtest.R
import br.com.digio.androidtest.di.baseTestModule
import br.com.digio.androidtest.espresso.IdlingResource
import br.com.digio.androidtest.matchers.isDisplayed
import br.com.digio.androidtest.matchers.matchText
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.koin.core.context.loadKoinModules
import org.koin.core.context.unloadKoinModules

class MainActivityTest {

    private val server = MockWebServer()

    private val context = InstrumentationRegistry.getInstrumentation().targetContext

    @Before
    fun setUp() {
        registerIdlingResource()
        loadKoinTestModules()
        server.start()
    }

    @After
    fun tearDown() {
        unregisterIdlingResource()
        unloadKoinTestModules()
        server.shutdown()
    }

    @Test
    fun titleShouldDisplayGreetings_whenActivityIsReady() {
        launchActivity<MainActivity>().apply {
            val title = context.getString(R.string.hello_maria)

            moveToState(Lifecycle.State.RESUMED)

            onView(withText(title)).check(matches(isDisplayed()))
        }
    }

    @Test
    fun shouldDisplaySpotlightList_whenDataIsSuccessfullyRetrievedFromServer() {
        server.dispatcher = MockServerUtil.dispatcherSuccess
        server.start(MockServerUtil.PORT)

        launchActivity<MainActivity>().apply {
            // TODO("validate if list displays items returned by server")
        }

        server.close()
    }

    @Test
    fun koinTest() {
        server.enqueue(MockServerUtil.errorResponse)

        launchActivity<MainActivity>().apply {
            moveToState(Lifecycle.State.RESUMED)

            onView(withId(android.R.id.message))

            onView(withId(R.id.txtMainDigioCash))
                .isDisplayed()
                .matchText(R.string.digio_cash)
        }
    }

    private fun registerIdlingResource() {
        IdlingRegistry.getInstance().register(IdlingResource.counter)
    }

    private fun unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(IdlingResource.counter)
    }

    private fun loadKoinTestModules() {
        loadKoinModules(baseTestModule)
    }

    private fun unloadKoinTestModules() {
        unloadKoinModules(baseTestModule)
    }
}