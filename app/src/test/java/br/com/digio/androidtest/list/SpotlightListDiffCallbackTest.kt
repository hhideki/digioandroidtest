package br.com.digio.androidtest.list

import br.com.digio.androidtest.BaseTest
import br.com.digio.androidtest.model.Spotlight
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class SpotlightListDiffCallbackTest : BaseTest() {

    @Test
    fun `Should compare lists with different sizes`() {
        val listA = createListWithOneItem()
        val listB = createFirstListWithMultipleItems()
        val callback = SpotlightListDiffCallback(listA, listB)

        assertThat(callback.oldListSize, `is`(1))
        assertThat(callback.newListSize, `is`(3))
        assertThat(callback.areItemsTheSame(0, 0), `is`(false))
        assertThat(callback.areContentsTheSame(0, 0), `is`(true))
        assertThat(callback.areContentsTheSame(0, 1), `is`(true))
    }

    @Test
    fun `Should compare the same list`() {
        val list = createListWithOneItem()
        val callback = SpotlightListDiffCallback(list, list)

        assertThat(callback.oldListSize, `is`(1))
        assertThat(callback.newListSize, `is`(1))
        assertThat(callback.areItemsTheSame(0, 0), `is`(true))
        assertThat(callback.areContentsTheSame(0, 0), `is`(true))
        assertThat(callback.areContentsTheSame(0, 1), `is`(true))
    }

    @Test
    fun `Should compare different lists with the same size`() {
        val listA = createFirstListWithMultipleItems()
        val listB = createSecondListWithMultipleItems()
        val callback = SpotlightListDiffCallback(listA, listB)

        assertThat(callback.oldListSize, `is`(3))
        assertThat(callback.newListSize, `is`(3))
        assertThat(callback.areItemsTheSame(0, 0), `is`(false))
        assertThat(callback.areItemsTheSame(1, 1), `is`(false))
        assertThat(callback.areItemsTheSame(2, 2), `is`(false))
        assertThat(callback.areContentsTheSame(0, 0), `is`(true))
    }

    private fun createListWithOneItem() = listOf(
        Spotlight("banner URL", "name", "description")
    )

    private fun createFirstListWithMultipleItems() = listOf(
        Spotlight("banner URL 1", "name 1", "description`1"),
        Spotlight("banner URL 2", "name 2", "description`2"),
        Spotlight("banner URL 3", "name 3", "description`3")
    )

    private fun createSecondListWithMultipleItems() = listOf(
        Spotlight("another banner URL 1", "another name 1", "another description`1"),
        Spotlight("another banner URL 2", "another name 2", "another description`2"),
        Spotlight("another banner URL 3", "another name 3", "another description`3")
    )

}