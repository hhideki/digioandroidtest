package br.com.digio.androidtest.repository

import br.com.digio.androidtest.BaseTest
import br.com.digio.androidtest.datasource.DigioProductsDataSource
import br.com.digio.androidtest.model.Cash
import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.network.Failure
import br.com.digio.androidtest.util.Either
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class DigioProductsRepositoryImplTest : BaseTest() {

    @Test
    fun `getDigioProducts should delegate call to the data source`() = runBlockingTest {
        // Given
        val dataSource = mockk<DigioProductsDataSource>(relaxed = true)
        val repository = DigioProductsRepositoryImpl(dataSource)

        // When
        repository.getDigioProducts()

        // Then
        coVerify(exactly = 1) { dataSource.getDigioProducts() }
    }

    @Test
    fun `getDigioProducts should return a failure when the data source returns a failure`() =
        runBlockingTest {
            // Given
            val dataSource = mockk<DigioProductsDataSource>()
            val repository = DigioProductsRepositoryImpl(dataSource)
            val expectedFailure = Failure.GenericHttp()

            coEvery { dataSource.getDigioProducts() } returns Either.failure(expectedFailure)

            // When
            var failure: Failure? = null
            val result = repository.getDigioProducts()

            result.fold(onFail = { failure = it })

            // Then
            coVerify(exactly = 1) { dataSource.getDigioProducts() }
            assertThat(result, `is`(instanceOf(Either.Fail::class.java)))
            assertThat(failure, `is`(expectedFailure))
        }

    @Test
    fun `getDigioProducts should return success when the data source returns valid data`() =
        runBlockingTest {
            // Given
            val dataSource = mockk<DigioProductsDataSource>()
            val repository = DigioProductsRepositoryImpl(dataSource)
            val expectedResult = createDigioProducts()

            coEvery { dataSource.getDigioProducts() } returns Either.success(expectedResult)

            // When
            var digioProducts: DigioProducts? = null
            val result = repository.getDigioProducts()

            result.fold(onSuccess = { digioProducts = it })

            // Then
            coVerify(exactly = 1) { dataSource.getDigioProducts() }
            assertThat(result, `is`(instanceOf(Either.Success::class.java)))
            assertThat(digioProducts, `is`(expectedResult))
        }

    private fun createDigioProducts() = DigioProducts(
        cash = Cash("bannerUrl", "title"),
        products = listOf(),
        spotlight = listOf()
    )

}