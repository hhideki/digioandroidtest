package br.com.digio.androidtest.presentation.viewmodel

import androidx.lifecycle.Observer
import br.com.digio.androidtest.BaseTest
import br.com.digio.androidtest.model.Cash
import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.model.Product
import br.com.digio.androidtest.model.Spotlight
import br.com.digio.androidtest.network.Failure
import br.com.digio.androidtest.presentation.viewaction.MainViewAction
import br.com.digio.androidtest.usecase.GetDigioProductsUseCase
import br.com.digio.androidtest.util.Either
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

class MainViewModelTest : BaseTest() {

    private val digioProductsObserver: Observer<DigioProducts> = mockk()
    private val productsObserver: Observer<List<Product>> = mockk()
    private val spotlightsObserver: Observer<List<Spotlight>> = mockk()
    private val errorObserver: Observer<String> = mockk()

    @Test
    fun `dispatchViewAction should delegate call to GetDigioProducts use case when the ViewAction is FetchDigioProducts`() =
        runBlockingTest {
            // Given
            val useCase = mockk<GetDigioProductsUseCase>()
            val viewModel = MainViewModel(Dispatchers.Default, useCase)

            registerObservers(viewModel)

            coEvery { useCase.execute() } returns getEmptyListsSuccess()

            // When
            viewModel.dispatchViewAction(MainViewAction.FetchDigioProducts)

            // Then
            coVerify(exactly = 1) { useCase.execute() }

            unregisterObservers(viewModel)
        }

    @Test
    fun `dispatchViewAction should post fetched products when the ViewAction is FetchDigioProducts and data is found`() =
        runBlockingTest {
            // Given
            val useCase = mockk<GetDigioProductsUseCase>()
            val viewModel = MainViewModel(Dispatchers.Default, useCase)

            registerObservers(viewModel)

            coEvery { useCase.execute() } returns getEmptyListsSuccess()

            // When
            viewModel.dispatchViewAction(MainViewAction.FetchDigioProducts)

            // Then
            coVerify(exactly = 1) {
                useCase.execute()
                digioProductsObserver.onChanged(ofType(DigioProducts::class))
            }

            coVerify(exactly = 0) {
                errorObserver.onChanged(any())
            }

            unregisterObservers(viewModel)
        }

    @Test
    fun `dispatchViewAction should post a failure when the ViewAction is FetchDigioProducts and an error occurs`() =
        runBlockingTest {
            // Given
            val useCase = mockk<GetDigioProductsUseCase>()
            val viewModel = MainViewModel(Dispatchers.Default, useCase)

            registerObservers(viewModel)

            coEvery { useCase.execute() } returns getGenericFailure()

            // When
            viewModel.dispatchViewAction(MainViewAction.FetchDigioProducts)

            // Then
            coVerify(exactly = 1) {
                useCase.execute()
                errorObserver.onChanged(any())
            }

            coVerify(exactly = 0) {
                digioProductsObserver.onChanged(any())
                productsObserver.onChanged(any())
                spotlightsObserver.onChanged(any())
            }

            unregisterObservers(viewModel)
        }

    private fun getEmptyListsSuccess() = Either.success(
        DigioProducts(
            cash = Cash("bannerURL", "title"),
            products = listOf(),
            spotlight = listOf()
        )
    )

    private fun getGenericFailure() = Either.failure(Failure.GenericHttp())

    private fun registerObservers(viewModel: MainViewModel) {
        viewModel.state.let { state ->
            state.digioProducts.observeForever(digioProductsObserver)
            state.products.observeForever(productsObserver)
            state.spotlights.observeForever(spotlightsObserver)
            state.error.observeForever(errorObserver)
        }
    }

    private fun unregisterObservers(viewModel: MainViewModel) {
        viewModel.state.let { state ->
            state.digioProducts.removeObserver(digioProductsObserver)
            state.products.removeObserver(productsObserver)
            state.spotlights.removeObserver(spotlightsObserver)
            state.error.removeObserver(errorObserver)
        }
    }

}