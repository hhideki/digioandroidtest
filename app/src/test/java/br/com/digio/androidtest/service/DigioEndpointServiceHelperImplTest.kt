package br.com.digio.androidtest.service

import br.com.digio.androidtest.BaseTest
import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.network.Failure
import br.com.digio.androidtest.util.Either
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import retrofit2.Response

class DigioEndpointServiceHelperImplTest : BaseTest() {

    @Test
    fun `getDigioProducts should return a failure when the service fails`() = runBlockingTest {
        // Given
        val service = mockk<DigioEndpointService>()
        val response = mockk<Response<DigioProducts>>(relaxed = true)
        val helper = DigioEndpointServiceHelperImpl(service)

        coEvery { response.isSuccessful } returns false
        coEvery { response.code() } returns 404
        coEvery { service.getProducts() } returns response

        // When
        var failure: Failure? = null
        val result = helper.getDigioProducts().apply {
            fold(onFail = { failure = it })
        }

        coVerify(exactly = 1) {
            service.getProducts()
            response.isSuccessful
            response.code()
        }
        assertThat(result, `is`(not(nullValue())))
        assertThat(result, `is`(instanceOf(Either.Fail::class.java)))
        assertThat(failure, `is`(not(nullValue())))
        assertThat(failure, `is`(instanceOf(Failure.PageNotFound::class.java)))
    }

    @Test
    fun `getDigioProducts should return a generic failure when the service throws an exception`() =
        runBlockingTest {
            // Given
            val service = mockk<DigioEndpointService>()
            val response = mockk<Response<DigioProducts>>()
            val helper = DigioEndpointServiceHelperImpl(service)

            coEvery { service.getProducts() } throws Exception()

            // When
            var failure: Failure? = null
            val result = helper.getDigioProducts().apply {
                fold(onFail = { failure = it })
            }

            coVerify(exactly = 1) {
                service.getProducts()
            }
            coVerify(exactly = 0) {
                response.isSuccessful
                response.code()
            }
            assertThat(result, `is`(not(nullValue())))
            assertThat(result, `is`(instanceOf(Either.Fail::class.java)))
            assertThat(failure, `is`(not(nullValue())))
            assertThat(failure, `is`(instanceOf(Failure.GenericHttp::class.java)))
        }

}