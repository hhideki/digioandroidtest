package br.com.digio.androidtest.cache

import br.com.digio.androidtest.BaseTest
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class CacheImplTest : BaseTest() {

    @Test
    fun `Shouldn't have data when not data has been set`() {
        // Given
        val sut = CacheImpl<Any>()

        // When
        val hasData = sut.hasData()
        val data = sut.get()

        // Then
        assertThat(hasData, `is`(false))
        assertThat(data, `is`(nullValue()))
    }

    @Test
    fun `get should return the same data that has been set when the data is requested`() {
        // Given
        val sut = CacheImpl<Any>()
        val cacheContent = "Test"

        // When
        sut.update(cacheContent)
        val hasData = sut.hasData()
        val data = sut.get()

        // Then
        assertThat(hasData, `is`(true))
        assertThat(data, `is`(not(nullValue())))
        assertThat(data, `is`(equalTo(cacheContent)))
    }

    @Test
    fun `get should return null when null has been set and the data is requested`() {
        // Given
        val sut = CacheImpl<Any>()

        // When
        sut.update(null)
        val hasData = sut.hasData()
        val data = sut.get()

        // Then
        assertThat(hasData, `is`(false))
        assertThat(data, `is`(nullValue()))
    }

}