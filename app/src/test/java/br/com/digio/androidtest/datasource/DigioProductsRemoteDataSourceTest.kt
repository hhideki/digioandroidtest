package br.com.digio.androidtest.datasource

import br.com.digio.androidtest.cache.Cache
import br.com.digio.androidtest.cache.CacheImpl
import br.com.digio.androidtest.model.Cash
import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.model.Product
import br.com.digio.androidtest.model.Spotlight
import br.com.digio.androidtest.service.DigioEndpointServiceHelper
import io.mockk.coVerify
import io.mockk.mockk
import io.mockk.mockkObject
import io.mockk.spyk
import io.mockk.verify
import io.mockk.verifySequence
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

@ExperimentalCoroutinesApi
class DigioProductsRemoteDataSourceTest {

    @Test
    fun `getDigioProducts should always check the cache`() = runBlockingTest {
        // Given
        val service = mockk<DigioEndpointServiceHelper>(relaxed = true)
        val cache = spyk(createEmptyCache())
        val sut = DigioProductsRemoteDataSource(service, cache)

        // When
        sut.getDigioProducts()

        // Then
        verify(exactly = 1) { cache.hasData() }
        coVerify(exactly = 1) { service.getDigioProducts() }
    }

    @Test
    fun `getDigioProducts should delegate call to service helper when the cache is empty`() =
        runBlockingTest {
            // Given
            val service = mockk<DigioEndpointServiceHelper>(relaxed = true)
            val cache = spyk(createEmptyCache())
            val sut = DigioProductsRemoteDataSource(service, cache)

            // When
            sut.getDigioProducts()

            // Then
            verify(exactly = 1) { cache.hasData() }
            coVerify(exactly = 1) { service.getDigioProducts() }
        }

    @Test
    fun `getDigioProducts should not delegate call to service helper when the cache is not empty`() =
        runBlockingTest {
            // Given
            val service = mockk<DigioEndpointServiceHelper>(relaxed = true)
            val cache = spyk(createCacheWithFilledLists())
            val sut = DigioProductsRemoteDataSource(service, cache)

            // When
            sut.getDigioProducts()

            // Then
            verifySequence {
                cache.hasData()
                cache.get()
            }
            coVerify(exactly = 0) { service.getDigioProducts() }
        }

    @Test
    fun `getDigioProducts should return cached data with empty lists when the cache has empty lists`() =
        runBlockingTest {
            // Given
            val service = mockk<DigioEndpointServiceHelper>()
            val cache = spyk(createCacheWithEmptyLists())
            val sut = DigioProductsRemoteDataSource(service, cache)

            // When
            var cash: Cash? = null
            var productsList: List<Product>? = null
            var spotlightList: List<Spotlight>? = null
            val digioProducts = sut.getDigioProducts().apply {
                fold(onSuccess = {
                    cash = it.cash
                    productsList = it.products
                    spotlightList = it.spotlight
                })
            }

            // Then
            verify(exactly = 1) { cache.get() }

            digioProducts.also {
                assertThat(it, `is`(not(nullValue())))
                assertThat(it.isSuccess, `is`(true))
                assertThat(cash, `is`(not(nullValue())))
                assertThat(productsList, `is`(not(nullValue())))
                assertThat(productsList?.size, `is`(0))
                assertThat(spotlightList, `is`(not(nullValue())))
                assertThat(spotlightList?.size, `is`(0))
            }
        }

    @Test
    fun `getDigioProducts should return cached data with non-empty lists when the cache has non-empty lists`() =
        runBlockingTest {
            // Given
            val service = mockk<DigioEndpointServiceHelper>()
            val cache = spyk(createCacheWithFilledLists())
            val sut = DigioProductsRemoteDataSource(service, cache)

            // When
            var cash: Cash? = null
            var productsList: List<Product>? = null
            var spotlightList: List<Spotlight>? = null
            val digioProducts = sut.getDigioProducts().apply {
                fold(onSuccess = {
                    cash = it.cash
                    productsList = it.products
                    spotlightList = it.spotlight
                })
            }

            // Then
            verify(exactly = 1) { cache.get() }

            digioProducts.also {
                assertThat(it, `is`(not(nullValue())))
                assertThat(it.isSuccess, `is`(true))
                assertThat(cash, `is`(not(nullValue())))
                assertThat(productsList, `is`(not(nullValue())))
                assertThat(productsList?.size, `is`(1))
                assertThat(spotlightList, `is`(not(nullValue())))
                assertThat(spotlightList?.size, `is`(1))
            }
        }

    private fun createEmptyCache(): Cache<DigioProducts> = CacheImpl()

    private fun createCacheWithEmptyLists(): Cache<DigioProducts> =
        CacheImpl<DigioProducts>().apply {
            update(
                DigioProducts(
                    Cash("bannerURL", "title"),
                    listOf(),
                    listOf()
                )
            )
        }

    private fun createCacheWithFilledLists(): Cache<DigioProducts> =
        CacheImpl<DigioProducts>().apply {
            update(
                DigioProducts(
                    Cash("bannerURL", "title"),
                    listOf(Product("imageURL", "name", "description")),
                    listOf(Spotlight("bannerURL", "name", "description"))
                )
            )
        }

}