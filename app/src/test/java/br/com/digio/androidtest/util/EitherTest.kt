package br.com.digio.androidtest.util

import br.com.digio.androidtest.BaseTest
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class EitherTest : BaseTest() {

    @Test
    fun `Validate a success`() {
        // Given
        val either: Either<Any, Any> = Either.success("Test")

        // When
        var successCount = 0
        var failCount = 0
        either.fold(
            onSuccess = { successCount++ },
            onFail = { failCount++ }
        )

        assertThat(either.isSuccess, `is`(true))
        assertThat(either.isFail, `is`(false))
        assertThat(either, `is`(instanceOf(Either.Success::class.java)))
        assertThat(successCount, `is`(1))
        assertThat(failCount, `is`(0))
    }

    @Test
    fun `Validate a failure`() {
        // Given
        val either: Either<Any, Any> = Either.failure("Test")

        // When
        var successCount = 0
        var failCount = 0
        either.fold(
            onSuccess = { successCount++ },
            onFail = { failCount++ }
        )

        assertThat(either.isSuccess, `is`(false))
        assertThat(either.isFail, `is`(true))
        assertThat(either, `is`(instanceOf(Either.Fail::class.java)))
        assertThat(successCount, `is`(0))
        assertThat(failCount, `is`(1))
    }

}