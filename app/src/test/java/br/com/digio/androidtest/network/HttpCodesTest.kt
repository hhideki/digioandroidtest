package br.com.digio.androidtest.network

import br.com.digio.androidtest.BaseTest
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class HttpCodesTest : BaseTest() {

    @Test
    fun `withCode should return OK when the code is 200`() {
        assertThat(HttpCodes.withCode(200), `is`(HttpCodes.OK))
    }

    @Test
    fun `withCode should return PAGE_NOT_FOUND when the code is 404`() {
        assertThat(HttpCodes.withCode(404), `is`(HttpCodes.PAGE_NOT_FOUND))
    }

    @Test
    fun `withCode should return GATEWAY_TIMEOUT when the code is 504`() {
        assertThat(HttpCodes.withCode(504), `is`(HttpCodes.GATEWAY_TIMEOUT))
    }

    @Test
    fun `withCode should return UNKNOWN when the code doesn't exist`() {
        assertThat(HttpCodes.withCode(999), `is`(HttpCodes.UNKNOWN))
    }

}