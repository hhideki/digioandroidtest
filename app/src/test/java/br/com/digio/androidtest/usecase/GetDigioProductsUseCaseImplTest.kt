package br.com.digio.androidtest.usecase

import br.com.digio.androidtest.BaseTest
import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.network.Failure
import br.com.digio.androidtest.repository.DigioProductsRepository
import br.com.digio.androidtest.util.Either
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.instanceOf
import org.hamcrest.CoreMatchers.not
import org.hamcrest.CoreMatchers.nullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class GetDigioProductsUseCaseImplTest : BaseTest() {

    @Test
    fun `execute should delegate call to the repository`() = runBlockingTest {
        // Given
        val repository = mockk<DigioProductsRepository>(relaxed = true)
        val useCase = GetDigioProductsUseCaseImpl(repository)

        // When
        useCase.execute()

        // Then
        coVerify(exactly = 1) { repository.getDigioProducts() }
    }

    @Test
    fun `execute should return a failure when the repository returns a failure`() =
        runBlockingTest {
            // Given
            val repository = mockk<DigioProductsRepository>()
            val useCase = GetDigioProductsUseCaseImpl(repository)
            val expectedFailure = Failure.GenericHttp()

            coEvery { repository.getDigioProducts() } returns Either.failure(expectedFailure)

            // When
            var failure: Failure? = null
            val result = useCase.execute().apply {
                fold(onFail = { failure = it })
            }

            // Then
            coVerify(exactly = 1) { repository.getDigioProducts() }
            assertThat(result, `is`(not(nullValue())))
            assertThat(result, `is`(instanceOf(Either.Fail::class.java)))
            assertThat(failure, `is`(not(nullValue())))
            assertThat(failure, `is`(instanceOf(Failure::class.java)))
            assertThat(failure, `is`(equalTo(expectedFailure)))
        }

    @Test
    fun `execute should return a success when the repository returns valid data`() =
        runBlockingTest {
            // Given
            val repository = mockk<DigioProductsRepository>()
            val useCase = GetDigioProductsUseCaseImpl(repository)
            val expectedDigioProducts = mockk<DigioProducts>()

            coEvery { repository.getDigioProducts() } returns Either.success(expectedDigioProducts)

            // When
            var digioProducts: DigioProducts? = null
            val result = useCase.execute().apply {
                fold(onSuccess = { digioProducts = it })
            }

            // Then
            coVerify(exactly = 1) { repository.getDigioProducts() }
            assertThat(result, `is`(not(nullValue())))
            assertThat(result, `is`(instanceOf(Either.Success::class.java)))
            assertThat(digioProducts, `is`(not(nullValue())))
            assertThat(digioProducts, `is`(instanceOf(DigioProducts::class.java)))
            assertThat(digioProducts, `is`(equalTo(expectedDigioProducts)))
        }

}