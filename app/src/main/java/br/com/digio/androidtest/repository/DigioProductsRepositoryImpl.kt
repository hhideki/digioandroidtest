package br.com.digio.androidtest.repository

import br.com.digio.androidtest.datasource.DigioProductsDataSource

class DigioProductsRepositoryImpl(
    private val remoteDataSource: DigioProductsDataSource
) : DigioProductsRepository {

    override suspend fun getDigioProducts() = remoteDataSource.getDigioProducts()


}