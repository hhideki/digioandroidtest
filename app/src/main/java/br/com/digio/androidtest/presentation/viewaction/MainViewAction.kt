package br.com.digio.androidtest.presentation.viewaction

sealed class MainViewAction {

    object FetchDigioProducts : MainViewAction()

}