package br.com.digio.androidtest.service

import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.network.Failure
import br.com.digio.androidtest.util.Either

interface DigioEndpointServiceHelper {

    suspend fun getDigioProducts(): Either<DigioProducts, Failure>

}