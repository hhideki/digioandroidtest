package br.com.digio.androidtest.repository

import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.network.Failure
import br.com.digio.androidtest.util.Either

interface DigioProductsRepository {

    suspend fun getDigioProducts(): Either<DigioProducts, Failure>

}