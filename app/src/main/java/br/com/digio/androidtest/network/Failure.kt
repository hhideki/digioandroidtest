package br.com.digio.androidtest.network

sealed class Failure(
    val code: Int,
    val message: String
) {

    constructor(httpCode: HttpCodes, message: String) : this(httpCode.code, message)

    class GenericHttp(
        httpCode: Int = HttpCodes.UNKNOWN.code,
        message: String = "Uh-oh, something went wrong!"
    ) : Failure(httpCode, message)

    class PageNotFound(message: String) : Failure(HttpCodes.PAGE_NOT_FOUND, message)
    class Timeout(message: String) : Failure(HttpCodes.GATEWAY_TIMEOUT, message)

    class EmptyCache : Failure(-1, "Cache not found")
}