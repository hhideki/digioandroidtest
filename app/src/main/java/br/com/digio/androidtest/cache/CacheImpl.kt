package br.com.digio.androidtest.cache

class CacheImpl<T> : Cache<T> {

    private var data: T? = null

    override fun get() = data

    override fun update(newData: T?) {
        data = newData
    }

    override fun hasData() = data != null

}