package br.com.digio.androidtest.datasource

import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.network.Failure
import br.com.digio.androidtest.util.Either

interface DigioProductsDataSource {

    suspend fun getDigioProducts(): Either<DigioProducts, Failure>

}