package br.com.digio.androidtest.model

data class Cash(
    val bannerURL: String,
    val title: String
)