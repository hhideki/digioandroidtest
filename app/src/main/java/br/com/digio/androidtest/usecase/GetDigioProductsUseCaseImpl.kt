package br.com.digio.androidtest.usecase

import br.com.digio.androidtest.repository.DigioProductsRepository

class GetDigioProductsUseCaseImpl(
    private val repository: DigioProductsRepository
) : GetDigioProductsUseCase {

    override suspend fun execute() = repository.getDigioProducts()

}