package br.com.digio.androidtest.presentation.viewstate

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.model.Product
import br.com.digio.androidtest.model.Spotlight

class MainViewState {

    val digioProducts = MutableLiveData<DigioProducts>()

    val products = Transformations.map(digioProducts) { it.products }

    val spotlights = Transformations.map(digioProducts) { it.spotlight }

    val error = MutableLiveData<String>()

}