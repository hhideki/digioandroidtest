package br.com.digio.androidtest.cache

interface Cache<T> {

    fun get(): T?

    fun update(data: T?)

    fun hasData(): Boolean

}