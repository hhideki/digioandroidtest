package br.com.digio.androidtest.service

import br.com.digio.androidtest.model.DigioProducts
import retrofit2.Response
import retrofit2.http.GET

interface DigioEndpointService {

    @GET("sandbox/products")
    suspend fun getProducts(): Response<DigioProducts>

}