package br.com.digio.androidtest.network

enum class HttpCodes(val family: Int, val code: Int) {

    UNKNOWN(0, 0),

    // Success
    OK(200, 200),

    // Client errors
    BAD_REQUEST(400, 400),
    UNAUTHORIZED(400, 401),
    FORBIDDEN(400, 403),
    PAGE_NOT_FOUND(400, 404),

    // Server errors
    INTERNAL_SERVER_ERROR(500, 500),
    BAD_GATEWAY(500, 502),
    GATEWAY_TIMEOUT(500, 504),
    AUTHENTICATION_REQUIRED(500, 511),

    ;

    companion object {
        fun withCode(code: Int) = values().firstOrNull { it.code == code } ?: UNKNOWN
    }

}