package br.com.digio.androidtest.datasource

import br.com.digio.androidtest.cache.Cache
import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.network.Failure
import br.com.digio.androidtest.service.DigioEndpointServiceHelper
import br.com.digio.androidtest.util.Either
import kotlinx.coroutines.delay

class DigioProductsRemoteDataSource(
    private val serviceHelper: DigioEndpointServiceHelper,
    private val cache: Cache<DigioProducts>
) : DigioProductsDataSource {

    override suspend fun getDigioProducts(): Either<DigioProducts, Failure> =
        Either.failure(Failure.GenericHttp())
//        if (cache.hasData()) {
//            Either.success(cache.get()!!)
//        } else {
//            serviceHelper.getDigioProducts().also {
//                it.fold(
//                    onSuccess = { data -> cache.update(data) }
//                )
//            }
//        }

}