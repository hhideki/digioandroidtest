package br.com.digio.androidtest.ui

import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.digio.androidtest.R
import br.com.digio.androidtest.databinding.ActivityMainBinding
import br.com.digio.androidtest.espresso.IdlingResource
import br.com.digio.androidtest.model.Product
import br.com.digio.androidtest.model.Spotlight
import br.com.digio.androidtest.presentation.viewaction.MainViewAction
import br.com.digio.androidtest.presentation.viewmodel.MainViewModel
import br.com.digio.androidtest.ui.adapter.ProductAdapter
import br.com.digio.androidtest.ui.adapter.SpotlightAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel by viewModel<MainViewModel>()

    private val productAdapter: ProductAdapter by lazy {
        ProductAdapter()
    }

    private val spotlightAdapter: SpotlightAdapter by lazy {
        SpotlightAdapter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setupObservers()
    }

    private fun setupObservers() {
        viewModel.state.apply {
            with(this@MainActivity) {
                digioProducts.observe(this) { showContent() }
                products.observe(this) { updateProducts(it) }
                spotlights.observe(this) { updateSpotlights(it) }
                error.observe(this) { showError(it) }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        setupProductsRecyclerView()
        setupSpotlightsRecyclerView()
        hideContent()
        setupDigioCashText()

        viewModel.dispatchViewAction(MainViewAction.FetchDigioProducts)
    }

    private fun setupProductsRecyclerView() {
        binding.recyMainProducts.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.recyMainProducts.adapter = productAdapter
    }

    private fun setupSpotlightsRecyclerView() {
        binding.recyMainSpotlight.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.recyMainSpotlight.adapter = spotlightAdapter
    }

    private fun hideContent() {
        binding.body.visibility = View.GONE
        binding.loadDigioContainer.visibility = View.VISIBLE
    }

    private fun showContent() {
        binding.loadDigioContainer.visibility = View.GONE
        binding.body.visibility = View.VISIBLE
        IdlingResource.increment()
    }

    private fun updateProducts(products: List<Product>) {
        productAdapter.products = products
        IdlingResource.decrement()
    }

    private fun updateSpotlights(spotlights: List<Spotlight>) {
        spotlightAdapter.spotlights = spotlights
        IdlingResource.decrement()
    }

    private fun showError(errorMessage: String) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT)
            .show()
        IdlingResource.decrement()
    }

    private fun setupDigioCashText() {
        val digioCacheText = "digio Cash"
        binding.txtMainDigioCash.text = SpannableString(digioCacheText).apply {
            setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(this@MainActivity, R.color.blue_darker)
                ),
                0,
                5,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            setSpan(
                ForegroundColorSpan(
                    ContextCompat.getColor(this@MainActivity, R.color.font_color_digio_cash)
                ),
                6,
                digioCacheText.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
    }
}