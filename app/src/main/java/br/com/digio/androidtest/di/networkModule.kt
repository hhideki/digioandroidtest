package br.com.digio.androidtest.di

import br.com.digio.androidtest.service.DigioEndpointService
import br.com.digio.androidtest.service.DigioEndpointServiceHelper
import br.com.digio.androidtest.service.DigioEndpointServiceHelperImpl
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val URL = "https://7hgi9vtkdc.execute-api.sa-east-1.amazonaws.com/"

internal val networkModule = module {
    single {
        OkHttpClient.Builder()
            .build()
    }

    single<Converter.Factory> {
        GsonConverterFactory
            .create(GsonBuilder().create())
    }

    single {
        Retrofit.Builder()
            .baseUrl(URL)
            .client(get())
            .addConverterFactory(get())
            .build()
    }

    single {
        get<Retrofit>()
            .create(DigioEndpointService::class.java)
    }

    single<DigioEndpointServiceHelper> {
        DigioEndpointServiceHelperImpl(get())
    }
}