package br.com.digio.androidtest.usecase

import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.network.Failure
import br.com.digio.androidtest.util.Either

interface GetDigioProductsUseCase : UseCaseWithoutParameters<Either<DigioProducts, Failure>>