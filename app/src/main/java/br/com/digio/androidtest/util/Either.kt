package br.com.digio.androidtest.util

sealed class Either<out SUCCESS, out FAIL> {

    companion object {
        fun <SUCCESS> success(data: SUCCESS) = Success(data)

        fun <FAIL> failure(data: FAIL) = Fail(data)
    }

    data class Success<out SUCCESS>(val data: SUCCESS) : Either<SUCCESS, Nothing>()
    data class Fail<out FAIL>(val data: FAIL) : Either<Nothing, FAIL>()

    val isSuccess: Boolean
        get() = this is Success<SUCCESS>

    val isFail: Boolean
        get() = this is Fail<FAIL>

    fun fold(
        onSuccess: (SUCCESS) -> Unit = {},
        onFail: (FAIL) -> Unit = {}
    ) =
        when (this) {
            is Success -> onSuccess(data)
            is Fail -> onFail(data)
        }

}
