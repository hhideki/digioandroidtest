package br.com.digio.androidtest.usecase

interface UseCaseWithoutParameters<T> {

    suspend fun execute(): T

}