package br.com.digio.androidtest.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.com.digio.androidtest.espresso.IdlingResource
import br.com.digio.androidtest.presentation.viewaction.MainViewAction
import br.com.digio.androidtest.presentation.viewstate.MainViewState
import br.com.digio.androidtest.usecase.GetDigioProductsUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch

class MainViewModel(
    private val io: CoroutineDispatcher,
    private val getDigioProductsUseCase: GetDigioProductsUseCase
) : ViewModel() {

    val state = MainViewState()

    fun dispatchViewAction(action: MainViewAction) {
        IdlingResource.increment()

        when (action) {
            MainViewAction.FetchDigioProducts -> fetchDigioProducts()
        }
    }

    private fun fetchDigioProducts() {
        viewModelScope.launch(io) {
            getDigioProductsUseCase.execute()
                .fold(
                    onSuccess = { state.digioProducts.postValue(it) },
                    onFail = { state.error.postValue(it.message) }
                )
        }
    }

}