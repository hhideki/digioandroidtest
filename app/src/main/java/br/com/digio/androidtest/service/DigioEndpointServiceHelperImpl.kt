package br.com.digio.androidtest.service

import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.network.Failure
import br.com.digio.androidtest.network.HttpCodes
import br.com.digio.androidtest.util.Either
import retrofit2.Response

class DigioEndpointServiceHelperImpl(
    private val service: DigioEndpointService
) : DigioEndpointServiceHelper {

    override suspend fun getDigioProducts() = applyToDigioProducts { it!! }

    private suspend fun <T> applyToDigioProducts(block: (DigioProducts?) -> T) =
        try {
            val response = service.getProducts()

            if (response.isSuccessful) {
                Either.success(block(response.body()))
            } else {
                Either.failure(getNetworkFailure(response))
            }
        } catch (exception: Exception) {
            Either.failure(Failure.GenericHttp())
        }

    private fun <T> getNetworkFailure(response: Response<T>): Failure =
        when (response.code()) {
            HttpCodes.PAGE_NOT_FOUND.code -> Failure.PageNotFound(response.message())
            HttpCodes.GATEWAY_TIMEOUT.code -> Failure.Timeout(response.message())
            else -> Failure.GenericHttp(response.code(), response.message())
        }

}