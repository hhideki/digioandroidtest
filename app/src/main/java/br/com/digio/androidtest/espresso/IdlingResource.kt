package br.com.digio.androidtest.espresso

import androidx.test.espresso.idling.CountingIdlingResource

object IdlingResource {

    private const val RESOURCE_NAME = "GLOBAL"

    @JvmField
    val counter = CountingIdlingResource(RESOURCE_NAME)

    fun increment() {
        counter.increment()
    }

    fun decrement() {
        if (!counter.isIdleNow) {
            counter.decrement()
        }
    }

}