package br.com.digio.androidtest.ui.viewholder

import androidx.recyclerview.widget.RecyclerView
import br.com.digio.androidtest.databinding.ItemMainSpotlightBinding
import br.com.digio.androidtest.model.Spotlight
import com.squareup.picasso.Picasso

class SpotlightItemViewHolder constructor(
    private val binding: ItemMainSpotlightBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(spotlight: Spotlight) {
        binding.spotlight = spotlight
        binding.picasso = Picasso.get()
        binding.progressView = binding.progressImage
    }
}
