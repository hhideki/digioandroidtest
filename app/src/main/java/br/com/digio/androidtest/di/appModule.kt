package br.com.digio.androidtest.di

import br.com.digio.androidtest.cache.Cache
import br.com.digio.androidtest.cache.CacheImpl
import br.com.digio.androidtest.datasource.DigioProductsDataSource
import br.com.digio.androidtest.datasource.DigioProductsRemoteDataSource
import br.com.digio.androidtest.model.DigioProducts
import br.com.digio.androidtest.presentation.viewmodel.MainViewModel
import br.com.digio.androidtest.presentation.viewstate.MainViewState
import br.com.digio.androidtest.repository.DigioProductsRepository
import br.com.digio.androidtest.repository.DigioProductsRepositoryImpl
import br.com.digio.androidtest.usecase.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

internal val appModule = module {
    // Coroutine dispatchers
    single(named("io")) { Dispatchers.IO }

    // Caches
    single<Cache<DigioProducts>> { CacheImpl() }

    // Data sources
    single<DigioProductsDataSource> { DigioProductsRemoteDataSource(get(), get()) }

    // Repositories
    single<DigioProductsRepository> { DigioProductsRepositoryImpl(get()) }

    // Use cases
    single<GetDigioProductsUseCase> { GetDigioProductsUseCaseImpl(get()) }

    // ViewModels
    viewModel { MainViewModel(get(named("io")), get()) }
}