package br.com.digio.androidtest.ui.viewholder

import androidx.recyclerview.widget.RecyclerView
import br.com.digio.androidtest.databinding.ItemMainProductsBinding
import br.com.digio.androidtest.model.Product
import com.squareup.picasso.Picasso

class ProductItemViewHolder constructor(
    private val binding: ItemMainProductsBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(product: Product) {
        binding.product = product
        binding.picasso = Picasso.get()
        binding.progressView = binding.progressImage
    }
}
