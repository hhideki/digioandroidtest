package br.com.digio.androidtest.app

import android.app.Application
import br.com.digio.androidtest.di.appModule
import br.com.digio.androidtest.di.networkModule
import org.koin.core.KoinComponent
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin

class DigioTestApp : Application(), KoinComponent {

    init {
        startKoin {
            loadKoinModules(networkModule)
            loadKoinModules(appModule)
        }
    }

}