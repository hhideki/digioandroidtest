package br.com.digio.androidtest.ui.bindingadapter

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

@BindingAdapter("imageUrl", "picasso", "loadingView", "errorDrawable", requireAll = true)
fun setImageUrl(
    view: ImageView,
    url: String,
    picasso: Picasso,
    progressView: View,
    errorDrawable: Drawable
) {
    picasso
        .load(url)
        .error(errorDrawable)
        .into(view, object : Callback {
            override fun onSuccess() {
                progressView.visibility = View.GONE
            }

            override fun onError(e: Exception?) {
                progressView.visibility = View.GONE
            }

        })
}